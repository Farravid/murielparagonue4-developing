// Fill out your copyright notice in the Description page of Project Settings.


#include "MurielCharacter.h"


// Sets default values
AMurielCharacter::AMurielCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMurielCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMurielCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AMurielCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

//QuickSortAlgorithm
void AMurielCharacter::QuickSortAlgorithm(UPARAM(ref) TArray<AActor *> &actors)
{
	QuickSortProcess(actors, 0, actors.Num() - 1);
}

void AMurielCharacter::QuickSortProcess(TArray<AActor *> &actors, int lStart, int rStart) {
	int lPointer = lStart;
	int rPointer = rStart;
	float pivot = actors[(lPointer + rPointer) / 2]->GetDistanceTo(this);
	AActor * temporal;

	do {
		while (actors[lPointer]->GetDistanceTo(this) < pivot) {
			++lPointer;
		}
		while (pivot < actors[rPointer]->GetDistanceTo(this)) {
			--rPointer;
		}
		if (lPointer <= rPointer) {
			temporal = actors[lPointer];
			actors[lPointer] = actors[rPointer];
			actors[rPointer] = temporal;
			++lPointer;
			--rPointer;
		}
	} while (lPointer <= rPointer);
	if (lStart < rPointer) {
		QuickSortProcess(actors, lStart, rPointer);
	}
	if (rStart > lPointer) {
		QuickSortProcess(actors, lPointer, rStart);
	}
}

void AMurielCharacter::PruebaPrint() {
	if (GEngine)
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Some debug message!"));
}

