// Fill out your copyright notice in the Description page of Project Settings.


#include "MyActor.h"

// Sets default values
AMyActor::AMyActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	TotalDamage = 200;
	TotalPoints = 10;

}

// Called when the game starts or when spawned
void AMyActor::BeginPlay()
{
	Super::BeginPlay();
	
}

void AMyActor::PostInitProperties() {
	Super::PostInitProperties();

	CalculateDefaultValues();
}

void AMyActor::CalculateDefaultValues() {
	Division = TotalDamage / TotalPoints;
}

#if WITH_EDITOR
void AMyActor::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) {
	CalculateDefaultValues();

	Super::PostEditChangeProperty(PropertyChangedEvent);
}
#endif

// Called every frame
void AMyActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

