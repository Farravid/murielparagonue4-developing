// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Math/Vector.h"
#include "Kismet/KismetSystemLibrary.h"
#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "MurielCharacter.generated.h"

UCLASS()
class FLYPARAGONCHARACTER_API AMurielCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMurielCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;


	UFUNCTION(BlueprintCallable, Category = "Sort")
	void QuickSortAlgorithm(UPARAM(ref) TArray<AActor*>& actors);

	void QuickSortProcess(TArray<AActor*>& actors, int lPointer, int rPointer);

	//PruebaPrint
	UFUNCTION(BlueprintCallable, Category = "DebugC++")
	void PruebaPrint();

};
