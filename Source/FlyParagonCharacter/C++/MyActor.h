// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MyActor.generated.h"

UCLASS()
class FLYPARAGONCHARACTER_API AMyActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage")
		int32 TotalDamage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage")
		int32 TotalPoints;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly,Transient, Category = "Damage")
		float Division;
	AMyActor();
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	void PostInitProperties();
	void PostEditChangeProperty(FPropertyChangedEvent & PropertyChangedEvenet);
	UFUNCTION(BlueprintCallable, Category = "Damage")
	void CalculateDefaultValues();



public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
